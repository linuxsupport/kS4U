%{!?dist: %define dist .slc5}
Name: @PACKAGE@
Version: @VERSION@
Release: @RELEASE@%{?dist}
Summary: kS4U - utility to acquire Kerberos credentials for User (S4U)
Group: Applications/System
Source: %{name}-%{version}.tar.gz
License: GPL
Vendor: CERN
Packager: Jaroslaw.Polok@cern.ch
URL: http://cern.ch/linux/

BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch

Requires: perl-Authen-Krb5 >= 1.9-1.el6.cern.1

%description 
%{name} is Kerberos Services 4 User (S4U) implementation in perl
(http://k5wiki.kerberos.org/wiki/Projects/Services4User)
note: it requires a perl-Authen-Krb5 version including this
patch: https://rt.cpan.org/Public/Bug/Display.html?id=89446


%prep
%setup -q

%build
make all

%install
mkdir -p $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT/

%clean
rm -rf $RPM_BUILD_ROOT

%files 
%defattr(-,root,root)
%{_bindir}/kS4U
%{_mandir}/man3/kS4U.3*
%doc README

%changelog
* Wed Mar  8 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.3-1
- adding multiple proxy credentials support (patch from Kuba Moscicki)

* Sun Oct 13 2013 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.1-1
- initial test release.
